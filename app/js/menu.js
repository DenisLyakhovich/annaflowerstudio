$(document).ready(function() {
  $('.menu-trigger').click(function() {
    $('nav ul').slideToggle(800);
  });
  
  $(window).resize(function() {		
		if (  $(window).width() > 800 ) {			
			$('nav ul').removeAttr('style');
		 }
	});
	
});
